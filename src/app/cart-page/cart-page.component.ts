import { Component, OnInit } from '@angular/core';
import { ProductService } from 'src/app/shared/product.service';
import { OrderService } from './../shared/order.service';
import { FormGroup, FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'app-cart-page',
  templateUrl: './cart-page.component.html',
  styleUrls: ['./cart-page.component.scss']
})
export class CartPageComponent implements OnInit {

  cartProducts = []
  totalPrice = 0
  added=''

  submitted = false
  form: FormGroup

  constructor(
    private productService : ProductService,
    private orderService : OrderService
  ) { }

  ngOnInit(): void {
  this.cartProducts = this.productService.cartProducts
    this.cartProducts.forEach(e=>{
      this.totalPrice += parseInt(e.price) 
    })

    this.form = new FormGroup({
      name: new FormControl(null, Validators.required),
      phone: new FormControl(null, Validators.required),
      address: new FormControl(null, Validators.required),
      payment: new FormControl(null, Validators.required),
    })
  }

  get name() { 
    return this.form.get('name');
  }
  get phone() {
     return this.form.get('phone');
    }
  get address() {
     return this.form.get('address');
    }
  get payment() { 
    return this.form.get('payment');
  }
  onSubmit(){
    if(this.form.invalid){
      return
    }
    this.submitted = true
    const order = {
      name: this.form.value.name,
      phone: this.form.value.phone,
      address: this.form.value.address,
      orders: this.cartProducts,
      payment: this.form.value.payment,
      price: this.totalPrice,
      date: new Date()
    }
    this.orderService.create(order).subscribe(res => {
      this.form.reset();
      this.added = 'Delivery is framed'
      this.submitted = false;
    })
  }

  deleteOrderProduct(product){
    this.totalPrice -= +product.price
    this.cartProducts.splice(this.cartProducts.indexOf(product), 1)
  }
}
