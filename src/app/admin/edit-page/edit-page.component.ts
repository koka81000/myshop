import { Product } from './../../shared/interfaces';
import { FormGroup, Validators, FormControl } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ProductService } from 'src/app/shared/product.service';
import { switchMap } from 'rxjs/operators';
import { ProductsType } from 'src/app/enums/product.enum';

@Component({
  selector: 'app-edit-page',
  templateUrl: './edit-page.component.html',
  styleUrls: ['./edit-page.component.scss']
})
export class EditPageComponent implements OnInit {

  PRODUCTS_TYPE = ProductsType
  form : FormGroup
  product : Product
  submitted = false
  constructor(
    private route: ActivatedRoute,
    private productService: ProductService,
    private router: Router
  ) { }

  ngOnInit(): void {
    this.route.params.pipe(
      switchMap ( params => {
        return this.productService.getProductDyId(params['id'])
      })
    ).subscribe( product =>{
      this.product = product
      this.form = new FormGroup({
        type: new FormControl(product.type, Validators.required),
        title: new FormControl(product.title, Validators.required),
        photo: new FormControl(product.photo, Validators.required),
        info: new FormControl(product.info, Validators.required),
        price: new FormControl(product.price, Validators.required),
      })
    })
  }

  onSubmit(){
    if(this.form.invalid){
      return
    }
    this.submitted = true
    this.productService.updateProduct({
      ...this.product,
      type: this.form.value.type,
      title: this.form.value.title,
      photo: this.form.value.photo,
      info: this.form.value.info,
      price: this.form.value.price,
      date: new Date(),
    }).subscribe(res => {
      this.submitted = false;
      this.router.navigate(['/admin','dashboard'])
    })
  }

  

  get type() { return this.form.get('type');}
  get title() { return this.form.get('title');}
  get photo() { return this.form.get('photo');}
  get info() { return this.form.get('info');}
  get price() { return this.form.get('price');}

}
