import { OrderService } from './../../shared/order.service';
import { Component, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-orders-page',
  templateUrl: './orders-page.component.html',
  styleUrls: ['./orders-page.component.scss']
})
export class OrdersPageComponent implements OnInit {

  orders = []
  pSub: Subscription
  rSub : Subscription

  constructor(
    private orderService:  OrderService
  ) { }

  ngOnInit(): void {
    this.pSub = this.orderService.getOrders().subscribe( orders =>{
      this.orders = orders
    })
  }

  ngOnDestroy(){
    if(this.pSub) {
      this.pSub.unsubscribe()
    }
    if(this.rSub) {
      this.rSub.unsubscribe()
    }
  }

  delete (id) {
    this.rSub = this.orderService.deleteOrder(id).subscribe( () => {
      this.orders = this.orders.filter( order => order.id !== id)
    })
  }

}
