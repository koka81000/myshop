import { Router } from '@angular/router';
import { ProductService } from './../../shared/product.service';
import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { from } from 'rxjs';
import {ProductsType} from '../../enums/product.enum'
@Component({
  selector: 'app-add-page',
  templateUrl: './add-page.component.html',
  styleUrls: ['./add-page.component.scss']
})


export class AddPageComponent implements OnInit {
  PRODUCTS_TYPE = ProductsType
  form :FormGroup
  submitted = false
  constructor(
    private productService: ProductService,
    private router: Router,
  ) { }

  ngOnInit(): void {

    this.form = new FormGroup({
      type: new FormControl(null, Validators.required),
      title: new FormControl(null, Validators.required),
      photo: new FormControl(null, Validators.required),
      info: new FormControl(null, Validators.required),
      price: new FormControl(null, Validators.required),
    })
  }

  get type() { return this.form.get('type');}
  get title() { return this.form.get('title');}
  get photo() { return this.form.get('photo');}
  get info() { return this.form.get('info');}
  get price() { return this.form.get('price');}


  onSubmit(){
    if(this.form.invalid){
      return
    }
    this.submitted = true
    const product = {
      type: this.form.value.type,
      title: this.form.value.title,
      photo: this.form.value.photo,
      info: this.form.value.info,
      price: this.form.value.price,
      date: new Date(),
    }
    this.productService.create(product).subscribe(res => {
      this.form.reset();
      this.submitted = false;
      this.router.navigate(['/'])
    })
  }
}
