import { Component, OnInit } from '@angular/core';
import { ProductService } from 'src/app/shared/product.service';
import { Product } from 'src/app/shared/interfaces';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {

  products: Product[] = []
  pSub: Subscription
  rSub : Subscription
  productName =''

  constructor(
    private productService:  ProductService
  ) { }

  ngOnInit(): void {
    this.pSub = this.productService.getProducts().subscribe( products =>{     
      this.products = products
    })
  }

  ngOnDestroy(){
    if(this.pSub) {
      this.pSub.unsubscribe()
    }
    if(this.rSub) {
      this.rSub.unsubscribe()
    }
  }

  delete (id) {
    this.rSub = this.productService.deleteProduct(id).subscribe( () => {
      this.products = this.products.filter( product => product.id !== id)
    })
  }
}
