import { Component, OnInit } from '@angular/core';
import { ProductService } from '../shared/product.service';
import { ActivatedRoute } from '@angular/router';
import { switchMap } from 'rxjs/operators';
import { Product } from '../shared/interfaces';
import { ProductsType } from '../enums/product.enum';

@Component({
  selector: 'app-product-page',
  templateUrl: './product-page.component.html',
  styleUrls: ['./product-page.component.scss']
})
export class ProductPageComponent implements OnInit {

product$

  constructor(
    private productService: ProductService,
    private route: ActivatedRoute

  ) { }

  ngOnInit(): void {
    this.product$ = this.route.params
    .pipe( switchMap( params => {
      return this.productService.getProductDyId(params['id'])
    }))
  }
  addToCart(product){
    this.productService.addProductToCart(product)
  }
  
}
